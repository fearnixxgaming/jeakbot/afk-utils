package de.fearnixx.afkutils.api.state;

public interface IClientLeaveAFK extends IAFKEvent {

    LeaveAFKReason getReason();

    enum LeaveAFKReason {
        IDLE_RESET,
        STATUS_UNSET,
        CHANNEL_LEFT,
        DISCONNECT_PAUSE,
        DISCONNECT,
        OTHER
    }
}
