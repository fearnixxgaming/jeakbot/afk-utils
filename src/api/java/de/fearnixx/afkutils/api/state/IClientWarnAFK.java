package de.fearnixx.afkutils.api.state;

public interface IClientWarnAFK extends IAFKEvent {

    Integer getIdleRemaining();
}
