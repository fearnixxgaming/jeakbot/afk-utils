package de.fearnixx.afkutils.watcher;

import de.fearnixx.afkutils.AFKService;
import de.fearnixx.afkutils.api.state.IClientLeaveAFK.LeaveAFKReason;
import de.fearnixx.afkutils.state.ClientLeaveAFK;
import de.fearnixx.jeak.event.IQueryEvent;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.Listener;
import de.fearnixx.jeak.service.event.IEventService;
import de.fearnixx.jeak.teamspeak.data.IClient;

import java.time.LocalDateTime;

/**
 * This watcher is responsible for cancelling AFK states when different things happen
 * Not tracked: Pausing AFK due to disconnects.
 */
public class CancelAFKWatcher {

    @Inject
    private IEventService eventService;

    private AFKService afkService;

    private final Object STATE_LOCK;

    public CancelAFKWatcher(AFKService afkService) {
        this.afkService = afkService;
        STATE_LOCK = afkService.getSTATE_LOCK();
    }

    @Listener
    public void onClientMoved(IQueryEvent.INotification.IClientMoved event) {
        // Only clients who changed the channel by themselves cancel the afk time
        if ("0".equals(event.getProperty("reasonid").orElse("0"))) {
            IClient client = event.getTarget();

            afkService.getState(client.getClientID()).ifPresent(state -> {
                synchronized (STATE_LOCK) {
                    if (state.isAfk()) {
                        // Client is tracked as AFK -> cancel.
                        state.setAfk(false);
                        state.setEnterReason(null);
                        state.setLeaveReason(LeaveAFKReason.CHANNEL_LEFT);
                        state.setExpiry(LocalDateTime.now().plusMinutes(20));

                        ClientLeaveAFK leaveAFK = new ClientLeaveAFK(client, LeaveAFKReason.CHANNEL_LEFT);
                        eventService.fireEvent(leaveAFK);
                    }
                }
            });
        }
    }

    @Listener
    public void onClientLeave(IQueryEvent.INotification.IClientLeave event) {
        Integer reasonId = event.getProperty("reasonid")
                .map(Integer::parseInt)
                .orElse(0);

        if (reasonId != 3) {
            // Handle all disconnects - except CONNECTION_LOST
            afkService.getState(event.getTarget().getClientID()).ifPresent(state -> {
                synchronized (STATE_LOCK) {
                    state.setAfk(false);
                    state.setEnterReason(null);
                    state.setLeaveReason(LeaveAFKReason.DISCONNECT);
                    state.setExpiry(LocalDateTime.now());

                    ClientLeaveAFK leaveAFK = new ClientLeaveAFK(event.getTarget(), LeaveAFKReason.DISCONNECT);
                    eventService.fireEvent(leaveAFK);
                    afkService.untrackState(state);
                }
            });
        }
    }
}
