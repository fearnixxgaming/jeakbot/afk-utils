package de.fearnixx.afkutils.watcher;

import de.fearnixx.afkutils.AFKConfiguration;
import de.fearnixx.afkutils.AFKService;
import de.fearnixx.afkutils.api.state.IAFKEvent;
import de.fearnixx.afkutils.api.state.IClientEnterAFK;
import de.fearnixx.afkutils.api.state.IClientLeaveAFK;
import de.fearnixx.afkutils.state.AFKState;
import de.fearnixx.afkutils.state.ClientEnterAFK;
import de.fearnixx.afkutils.state.ClientLeaveAFK;
import de.fearnixx.afkutils.state.ClientWarnAFK;
import de.fearnixx.jeak.event.IEvent;
import de.fearnixx.jeak.event.IQueryEvent;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.Listener;
import de.fearnixx.jeak.service.event.IEventService;
import de.fearnixx.jeak.teamspeak.cache.IDataCache;
import de.fearnixx.jeak.teamspeak.data.IClient;
import de.mlessmann.confort.api.IConfigNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * Responsible for updating clients AFK status based on the {@code clientlist} response of TS3.
 */
public class UpdateAFKWatcher {

    private static final Logger logger = LoggerFactory.getLogger(UpdateAFKWatcher.class);

    @Inject
    private IDataCache dataCache;

    @Inject
    private IEventService eventService;

    private final AFKService afkService;
    private final AFKConfiguration afkConfiguration;

    private final Object STATE_LOCK;

    public UpdateAFKWatcher(AFKService afkService) {
        this.afkService = afkService;
        STATE_LOCK = afkService.getSTATE_LOCK();
        afkConfiguration = afkService.getConfiguration();
    }

    @Listener
    public void onClientsUpdated(IQueryEvent.IDataEvent.IRefreshClients event) {
        final List<IEvent> scheduledEvents = new LinkedList<>();
        Consumer<IAFKEvent> eventConsumer = ev -> {
            synchronized (scheduledEvents) {
                scheduledEvents.add(ev);
            }
        };

        synchronized (STATE_LOCK) {
            dataCache.getClients()
                    .stream()
                    .filter(this::shouldCheckClient)
                    .forEach(cl -> checkClientUpdate(cl, eventConsumer));
        }
        scheduledEvents.forEach(eventService::fireEvent);
    }

    private boolean shouldCheckClient(IClient client) {
        boolean inIgnoredChannel = afkService.isIgnoredChannel(client.getChannelID(), true);
        boolean memberOfIgnoredGroup =
                client.getGroupIDs().stream().anyMatch(afkConfiguration.getIgnoredGroups()::contains);

        boolean shouldCheck = !inIgnoredChannel && !memberOfIgnoredGroup;
        if (logger.isDebugEnabled() && !shouldCheck) {
            logger.debug("Ignoring client: {} [c:{},sg:{}]", client, inIgnoredChannel, memberOfIgnoredGroup);
        }
        return shouldCheck;
    }

    private void checkClientUpdate(IClient client, Consumer<IAFKEvent> eventConsumer) {
        AFKState afkState = afkService.getState(client.getClientID()).orElse(null);

        // => Gather data for decisions
        int maxIdle = getClientMaxIdleTime(client);
        double currentIdle = Math.floor(MILLISECONDS.toSeconds(client.getIdleTime()));
        boolean isTrackedAfk = afkState != null && afkState.isAfk();
        boolean isAfkByIdle = currentIdle > maxIdle;
        boolean isAFKByAway = client.isAway();

        if (afkState == null && (isAfkByIdle || isAFKByAway)) {
            afkState = new AFKState(client.getClientUniqueID());
            afkService.trackState(client.getClientID(), afkState);
        }

        // => Decisions

        if (!isTrackedAfk) {
            // === Not-tracked-branch ===

            if (isAFKByAway) {
                // Client is now AFK for setting the AFK state.
                afkState.setAfk(true);
                afkState.setEnterReason(IClientEnterAFK.EnterAFKReason.STATUS_SET);
                afkState.setLeaveReason(null);
                ClientEnterAFK enterAFK = new ClientEnterAFK(client, afkState.getEnterReason());
                eventConsumer.accept(enterAFK);
                logger.debug("Client afk by flag: {}", client);

            } else if (isAfkByIdle) {
                // Client is now AFK for being idle too long.
                afkState.setAfk(true);
                afkState.setEnterReason(IClientEnterAFK.EnterAFKReason.IDLE_EXCEEDED);
                afkState.setLeaveReason(null);
                ClientEnterAFK enterAFK = new ClientEnterAFK(client, afkState.getEnterReason());
                eventConsumer.accept(enterAFK);
                logger.debug("Client afk for being idle: {} at {}/{} mic:{} sound:{}",
                        client, maxIdle, currentIdle, client.hasMicMuted(), client.hasOutputMuted());

            } else {
                // Client is not yet AFK.
                checkForAFKWarn(client, currentIdle, maxIdle, eventConsumer);
            }
        } else {
            // === Tracked-branch ===

            if (!isAfkByIdle && !isAFKByAway) {
                // Client is not AFK by idle or flag -> reset may be needed.

                // Case: Client is tracked AFK for re-connecting:
                boolean trackedByReconnect =
                        afkState.getEnterReason().equals(IClientEnterAFK.EnterAFKReason.CONNECT_RESUME);
                // Case: Client is tracked AFK for entering an AFK channel
                boolean trackedByChannel =
                        afkState.getEnterReason().equals(IClientEnterAFK.EnterAFKReason.CHANNEL_ENTERED);

                if (!trackedByReconnect && !trackedByChannel) {
                    // Client is not intentionally tracked -> Reset AFK state
                    afkState.setAfk(false);
                    afkState.setEnterReason(null);
                    afkState.setLeaveReason(IClientLeaveAFK.LeaveAFKReason.IDLE_RESET);
                    ClientLeaveAFK leaveAFK = new ClientLeaveAFK(client, afkState.getLeaveReason());
                    eventConsumer.accept(leaveAFK);
                    logger.debug("Client no longer deemed AFK: {}", client);
                } else {
                    logger.debug("Not un-afk-ing client for not being tracked by idle: {}. Reconnect: {}. Entered: {}.",
                            client, trackedByReconnect, trackedByChannel);
                }

            } else if (logger.isDebugEnabled()) {
                // Debugging information.

                if (isAfkByIdle) {
                    logger.debug("Client still idle-afk: {}", client);

                } else //noinspection ConstantConditions
                    if (isAFKByAway) {
                        logger.debug("Client still flag-afk: {}", client);
                    }
            }
        }
    }

    private void checkForAFKWarn(IClient client, double currentIdle, int maxIdle, Consumer<IAFKEvent> eventConsumer) {
        int refreshSeconds = dataCache.getClientRefreshTime();
        double remainingIdle = maxIdle - currentIdle;

        if (remainingIdle <= refreshSeconds) {
            logger.debug("Remaining idle {} <= {}. Warning client: {}", remainingIdle, refreshSeconds, client);
            ClientWarnAFK warnAFK = new ClientWarnAFK(client, ((int) remainingIdle));
            eventConsumer.accept(warnAFK);
        } else {
            logger.debug("Not warning client: {} - enough time left.", client);
        }
    }

    private int getClientMaxIdleTime(IClient client) {
        final int[] maxIdle = new int[]{0};

        client.getGroupIDs().forEach(id -> {
            if (maxIdle[0] < 0) return;
            IConfigNode groupIdle = afkConfiguration.getMaxIdles().getOrDefault(id.toString(), null);

            if (groupIdle != null) {
                int gIdleTime = groupIdle.optInteger().orElse(0);
                if (gIdleTime < 0 || gIdleTime > maxIdle[0])
                    maxIdle[0] = gIdleTime;
            }
        });

        if (maxIdle[0] == 0) {
            maxIdle[0] = afkConfiguration.getMaxIdles().get("default").optInteger().orElse(300);
        } else if (maxIdle[0] < 0) {
            return Integer.MAX_VALUE;
        }

        boolean micMuted = client.hasMicMuted();
        boolean outputMuted = client.hasOutputMuted();
        if (micMuted && outputMuted) {
            maxIdle[0] /= afkConfiguration.getBothMutedDivisor();
        } else if (outputMuted) {
            maxIdle[0] /= afkConfiguration.getOutputMutedDivisor();
        }

        return maxIdle[0];
    }
}
