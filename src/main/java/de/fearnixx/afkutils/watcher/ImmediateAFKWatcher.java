package de.fearnixx.afkutils.watcher;

import de.fearnixx.afkutils.AFKAutomationPlugin;
import de.fearnixx.afkutils.AFKConfiguration;
import de.fearnixx.afkutils.AFKService;
import de.fearnixx.afkutils.api.state.IClientEnterAFK;
import de.fearnixx.afkutils.state.AFKState;
import de.fearnixx.afkutils.state.ClientEnterAFK;
import de.fearnixx.jeak.event.IQueryEvent;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.Listener;
import de.fearnixx.jeak.service.event.IEventService;
import de.fearnixx.jeak.teamspeak.data.IClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Responsible for immediately setting a client AFK on certain actions
 */
public class ImmediateAFKWatcher {

    private static final Logger logger = LoggerFactory.getLogger(ImmediateAFKWatcher.class);

    private AFKService afkService;

    @Inject
    private IEventService eventService;

    private final Object STATE_LOCK;

    public ImmediateAFKWatcher(AFKService afkService) {
        this.afkService = afkService;
        this.STATE_LOCK = afkService.getSTATE_LOCK();
    }

    @Listener
    public void onClientMove(IQueryEvent.INotification.IClientMoved event) {
        final Integer targetChannelId = event.getTargetChannelId();
        enterChannel(targetChannelId, event.getTarget());
    }

    @Listener
    public void onClientEnter(IQueryEvent.INotification.IClientEnter event) {
        final Integer targetChannelId = event.getTargetChannelId();
        enterChannel(targetChannelId, event.getTarget());
    }

    private void enterChannel(Integer targetChannelId, IClient client) {
        if (afkService.isAFKChannel(targetChannelId)) {
            synchronized (STATE_LOCK) {
                AFKState afkState = afkService.getState(client.getClientID())
                        .orElseGet(() -> {
                            AFKState state = new AFKState(client.getClientUniqueID());
                            afkService.trackState(client.getClientID(), state);
                            return state;
                        });

                if (!afkState.isAfk()) {
                    logger.debug("Client entering afk channel: {} - setting AFK.", client);
                    afkState.setAfk(true);
                    afkState.setLeaveReason(null);
                    afkState.setEnterReason(IClientEnterAFK.EnterAFKReason.CHANNEL_ENTERED);
                    ClientEnterAFK enterAFK = new ClientEnterAFK(client, afkState.getEnterReason());
                    client.setProperty(AFKAutomationPlugin.IDENT_REASON, afkState.getEnterReason());
                    client.setProperty(AFKAutomationPlugin.IDENT_MOVED_FROM, targetChannelId);

                        eventService.fireEvent(enterAFK);
                    }
                }
            }
    }
}
