package de.fearnixx.afkutils.action;

import de.fearnixx.afkutils.AFKAutomationPlugin;
import de.fearnixx.afkutils.AFKConfiguration;
import de.fearnixx.afkutils.api.IAFKService;
import de.fearnixx.afkutils.api.state.IClientEnterAFK;
import de.fearnixx.afkutils.api.state.IClientLeaveAFK;
import de.fearnixx.afkutils.api.state.IClientLeaveAFK.LeaveAFKReason;
import de.fearnixx.afkutils.api.state.IClientWarnAFK;
import de.fearnixx.jeak.IBot;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.Listener;
import de.fearnixx.jeak.teamspeak.cache.IDataCache;
import de.fearnixx.jeak.teamspeak.data.IChannel;
import de.fearnixx.jeak.teamspeak.data.IClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Optional;

import static de.fearnixx.afkutils.api.state.IClientEnterAFK.EnterAFKReason.IDLE_EXCEEDED;
import static de.fearnixx.afkutils.api.state.IClientEnterAFK.EnterAFKReason.STATUS_SET;

public class AFKActionListener {

    private static final Logger logger = LoggerFactory.getLogger(AFKActionListener.class);

    @Inject
    private IAFKService afkService;

    @Inject
    private IBot bot;

    @Inject
    private IDataCache dataCache;

    private final AFKConfiguration config;

    public AFKActionListener(AFKConfiguration config) {
        this.config = config;
    }

    @Listener(order = Listener.Orders.EARLY)
    public void onWarnClient(IClientWarnAFK event) {

        Integer idleRemaining = event.getIdleRemaining();

        // If the move time is below the refresh threshold,
        // the move will take longer than the remaining idle time!
        int moveTime = Math.max(idleRemaining, dataCache.getClientRefreshTime());

        String message = config.getIdleWarnMessage(
                Collections.singletonMap("seconds", Integer.toString(moveTime)),
                event.getTarget().getCountryCode());

        sendMessage(event.getTarget(), message);
    }

    @Listener(order = Listener.Orders.EARLY)
    public void onClientAfk(IClientEnterAFK event) {
        IClient client = event.getTarget();
        if (event.getReason() == IDLE_EXCEEDED || event.getReason() == STATUS_SET) {
            if (!afkService.isAFKChannel(client.getChannelID())) {
                int channel = afkService.getSuitableAFKChannel(client);
                if (channel >= 0) {
                    client.setProperty(AFKAutomationPlugin.IDENT_MOVED_FROM, client.getChannelID());
                    final var request = client.moveToChannel(channel);
                    request.onError(answer -> logger.warn("Failed to move client to AFK-Channel \"{}\" {}: {} {}",
                            channel, client, answer.getError().getCode(), answer.getError().getMessage()));
                    request.onSuccess(answer -> {
                        String message = config.getMovedMessage(
                                Collections.singletonMap("channel", getChannelName(channel)),
                                client.getCountryCode());
                        sendMessage(client, message);
                    });
                    bot.getServer().getConnection().sendRequest(request);
                }
            }
        }
    }

    @Listener(order = Listener.Orders.EARLY)
    public void onClientNoLongerAfk(IClientLeaveAFK event) {
        IClient client = event.getTarget();

        if (event.getReason() == LeaveAFKReason.IDLE_RESET || event.getReason() == LeaveAFKReason.STATUS_UNSET
                || (event.getReason() == LeaveAFKReason.OTHER && afkService.isAFKChannel(client.getChannelID()))) {
            if (afkService.isAFKChannel(client.getChannelID())) {
                // Move client back - Schedule move from AFK channel
                Optional<String> movedFromProp = client.getProperty(AFKAutomationPlugin.IDENT_MOVED_FROM);

                if (movedFromProp.isPresent()) {
                    String movedFromChannel = movedFromProp.get();
                    int fromChannelId = Integer.parseInt(movedFromChannel);
                    logger.info("Client no longer afk - moving back: {}/{}", client.getClientUniqueID(), client.getNickName());

                    final var request = client.moveToChannel(fromChannelId);
                    request.onError(error -> logger.warn("Failed to move client {}}: {} {}",
                            client, error.getError().getCode(), error.getError().getMessage()));
                    request.onSuccess(answer -> {
                        String message = config.getMovedBackMessage(
                                Collections.singletonMap("channel", getChannelName(fromChannelId)),
                                client.getCountryCode());
                        sendMessage(client, message);
                    });
                    bot.getServer().getConnection().sendRequest(request);

                    // Client changed channel already - no move required.
                    client.setProperty(AFKAutomationPlugin.IDENT_MOVED_FROM, null);
                } else {
                    logger.debug("Not moving back client {}. No moved-from marker set.", client);
                }
            }
        }
    }

    private void sendMessage(IClient client, String message) {
        final var request = client.sendMessage(message);
        request.onError(error -> logger.warn("Message client failed! {}: {} {}",
                client, error.getError().getCode(), error.getError().getMessage()));
        bot.getServer().getConnection().sendRequest(request);
    }

    private String getChannelName(int channelId) {
        return Optional.ofNullable(dataCache.getChannelMap().getOrDefault(channelId, null))
                .map(IChannel::getName)
                .orElse("Unknown channel.");
    }
}
