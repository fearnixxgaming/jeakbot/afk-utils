package de.fearnixx.afkutils.state;

import de.fearnixx.afkutils.api.state.IClientLeaveAFK;
import de.fearnixx.jeak.teamspeak.data.IClient;

public class ClientLeaveAFK extends AFKEvent implements IClientLeaveAFK {

    private LeaveAFKReason leaveAFKReason;

    public ClientLeaveAFK(IClient target, LeaveAFKReason leaveAFKReason) {
        super(target);
        this.leaveAFKReason = leaveAFKReason;
    }

    @Override
    public LeaveAFKReason getReason() {
        return leaveAFKReason;
    }
}
