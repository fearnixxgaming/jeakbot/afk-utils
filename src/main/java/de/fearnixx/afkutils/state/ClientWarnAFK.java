package de.fearnixx.afkutils.state;

import de.fearnixx.afkutils.api.state.IClientWarnAFK;
import de.fearnixx.jeak.teamspeak.data.IClient;

public class ClientWarnAFK extends AFKEvent implements IClientWarnAFK {

    private final Integer timeRemaining;

    public ClientWarnAFK(IClient target, Integer timeRemaining) {
        super(target);
        this.timeRemaining = timeRemaining;
    }

    @Override
    public Integer getIdleRemaining() {
        return timeRemaining;
    }
}
