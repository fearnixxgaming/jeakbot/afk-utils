package de.fearnixx.afkutils.state;


import de.fearnixx.jeak.event.IEvent;
import de.fearnixx.jeak.teamspeak.data.IClient;

public class AFKEvent implements IEvent {

    private IClient target;

    public AFKEvent(IClient target) {
        this.target = target;
    }

    public IClient getTarget() {
        return target;
    }
}
