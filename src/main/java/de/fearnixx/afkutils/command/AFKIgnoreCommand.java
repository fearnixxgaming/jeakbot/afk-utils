package de.fearnixx.afkutils.command;

import de.fearnixx.afkutils.api.IAFKService;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.LocaleUnit;
import de.fearnixx.jeak.service.command.CommandException;
import de.fearnixx.jeak.service.command.ICommandContext;
import de.fearnixx.jeak.service.command.ICommandExecutionContext;
import de.fearnixx.jeak.service.command.ICommandReceiver;
import de.fearnixx.jeak.service.command.spec.Commands;
import de.fearnixx.jeak.service.command.spec.ICommandExecutor;
import de.fearnixx.jeak.service.command.spec.ICommandSpec;
import de.fearnixx.jeak.service.locale.ILocalizationUnit;
import de.fearnixx.jeak.service.permission.teamspeak.ITS3PermissionProvider;
import de.fearnixx.jeak.service.teamspeak.IUserService;
import de.fearnixx.jeak.teamspeak.PropertyKeys;
import de.fearnixx.jeak.teamspeak.data.IChannel;
import de.fearnixx.jeak.teamspeak.data.IClient;

import java.util.HashMap;
import java.util.Map;

import static de.fearnixx.jeak.service.command.spec.Commands.commandSpec;

public class AFKIgnoreCommand implements ICommandReceiver, ICommandExecutor {

    @Inject
    private IAFKService afkService;

    @Inject
    private ITS3PermissionProvider permissionProvider;

    @Inject
    private IUserService userSvc;

    @Inject
    @LocaleUnit("afk-utils")
    private ILocalizationUnit localizationUnit;

    public ICommandSpec getCommandSpec() {
        return commandSpec("afk-ignore", "afkutils:afk-ignore")
                .parameters(Commands.paramSpec("channel", IChannel.class))
                .executor(this)
                .permission("afkutils.commands.afk-ignore")
                .build();
    }

    @Override
    public void execute(ICommandExecutionContext ctx) {
        IChannel channel = ctx.getRequiredOne("channel", IChannel.class);
        final var msgParams = Map.of("channel", channel.toString());

        if (afkService.isIgnoredChannel(channel.getID(), false)) {
            afkService.unignoreChannel(channel.getID());
            String message = localizationUnit.getContext(ctx.getSender())
                    .optMessage("channel_unignored", msgParams)
                    .orElse("Channel unignored.");
            ctx.getConnection().sendRequest(ctx.getSender().sendMessage(message));

        } else {
            afkService.ignoreChannel(channel.getID());
            String message = localizationUnit.getContext(ctx.getSender())
                    .optMessage("channel_ignored", msgParams)
                    .orElse("Channel ignored.");
            ctx.getConnection().sendRequest(ctx.getSender().sendMessage(message));
        }
    }

    @Override
    public void receive(ICommandContext ctx) throws CommandException {
        int invokerId = ctx.getRawEvent().getProperty(PropertyKeys.TextMessage.SOURCE_ID)
                .map(Integer::parseInt)
                .orElseThrow(() -> new CommandException("Failed to get invoker uid!"));

        IClient client = userSvc.getClientByID(invokerId)
                .orElseThrow(() -> new CommandException("Could not find you in the cache. Please wait a moment and try again."));

        if (client.hasPermission("afkutils:ignore-channel")) {
            Integer channelID = client.getChannelID();
            Map<String, String> params = new HashMap<>();
            params.put("channel", Integer.toString(channelID));

            if (afkService.isIgnoredChannel(channelID, false)) {
                afkService.unignoreChannel(channelID);
                String message = localizationUnit.getContext(client.getCountryCode())
                        .optMessage("channel_unignored", params)
                        .orElse("Channel unignored.");
                ctx.getRawEvent().getConnection().sendRequest(client.sendMessage(message));

            } else {
                afkService.ignoreChannel(channelID);
                String message = localizationUnit.getContext(client.getCountryCode())
                        .optMessage("channel_ignored", params)
                        .orElse("Channel ignored.");
                ctx.getRawEvent().getConnection().sendRequest(client.sendMessage(message));
            }

        } else {
            throw new CommandException("You are not allowed to do this!");
        }
    }
}
