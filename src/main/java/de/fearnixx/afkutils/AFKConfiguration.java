package de.fearnixx.afkutils;


import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.LocaleUnit;
import de.fearnixx.jeak.service.locale.ILocalizationUnit;
import de.mlessmann.confort.api.IConfigNode;
import de.mlessmann.confort.api.IValueHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class AFKConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(AFKConfiguration.class);

    @Inject
    @LocaleUnit(value = "afk-utils", defaultResource = "afkmove/assets/lang/locale.json")
    private ILocalizationUnit localization;

    public static AFKConfiguration fromConfig(IConfigNode config) {
        AFKConfiguration configuration = new AFKConfiguration();

        config.getNode("channels")
                .optMap()
                .orElseThrow(() -> new IllegalStateException("Channels node is not a hub!"))
                .forEach((key, value) -> configuration.afkChannels.put(key, value));

        config.getNode("idle_times")
                .optMap()
                .orElseThrow(() -> new IllegalStateException("Idle times node is not a hub!"))
                .forEach((key, value) -> configuration.maxIdles.put(key, value));

        configuration.ignoredChannels = config.getNode("channel_blacklist")
                .optList()
                .orElseGet(Collections::emptyList)
                .stream()
                .map(IConfigNode::asInteger)
                .collect(Collectors.toList());

        configuration.onlyCheckChannels = config.getNode("channel_whitelist")
                .optList()
                .orElseGet(Collections::emptyList)
                .stream()
                .map(IConfigNode::asInteger)
                .collect(Collectors.toList());

        configuration.ignoredGroups = config.getNode("ignore_groups")
                .optList()
                .orElseGet(Collections::emptyList)
                .stream()
                .map(IValueHolder::asInteger)
                .collect(Collectors.toList());

        configuration.bothMutedDivisor = config.getNode("both_muted_divisor").optInteger().orElse(4);
        configuration.outputMutedDivisor = config.getNode("output_muted_divisor").optInteger().orElse(2);

        if (!config.getNode("idle_warn_time").isVirtual()) {
            logger.warn("Setting the idle_warn_time is no longer supported. Clients will be notified based on the cache-refresh period.");
        }


        if (config.getNode("messages", "idle_warn").optString(null) != null
                || config.getNode("messages", "move_back").optString(null) != null
                || config.getNode("messages", "move").optString(null) != null
                || config.getNode("idle_warn_message").optString(null) != null) {
            logger.error("Configuring messages via. the configuration is no longer supported. Please use the localization feature!");
        }

        return configuration;
    }

    private AFKConfiguration() {
    }

    private Map<String, IConfigNode> afkChannels = new HashMap<>();
    private Map<String, IConfigNode> maxIdles = new HashMap<>();
    private List<Integer> ignoredChannels;
    private List<Integer> onlyCheckChannels;
    private List<Integer> ignoredGroups;

    private int bothMutedDivisor;
    private int outputMutedDivisor;

    public List<Integer> getIgnoredGroups() {
        return ignoredGroups;
    }

    public Map<String, IConfigNode> getAfkChannels() {
        return afkChannels;
    }

    public Map<String, IConfigNode> getMaxIdles() {
        return maxIdles;
    }

    public List<Integer> getIgnoredChannels() {
        return ignoredChannels;
    }

    public List<Integer> getOnlyCheckChannels() {
        return onlyCheckChannels;
    }

    public int getBothMutedDivisor() {
        return bothMutedDivisor;
    }

    public int getOutputMutedDivisor() {
        return outputMutedDivisor;
    }

    public String getIdleWarnMessage(Map<String, String> params, String countryCode) {
        return localization.getContext(countryCode)
                .optMessage("warn_afk", params)
                .orElseGet(() -> {
                    logger.warn("Missing \"warn_afk\" localization message!");
                    return "If you stay AFK, you will be moved!";
                });
    }

    public String getMovedBackMessage(Map<String, String> params, String countryCode) {
        return localization.getContext(countryCode)
                .optMessage("moved_back", params)
                .orElseGet(() -> {
                    logger.warn("Missing \"moved_back\" localization message!");
                    return "You have been moved back!";
                });
    }

    public String getMovedMessage(Map<String, String> params, String countryCode) {
        return localization.getContext(countryCode)
                .optMessage("moved_afk", params)
                .orElseGet(() -> {
                    logger.warn("Missing \"moved_afk\" localization message!");
                    return "You have been moved for being afk.";
                });
    }
}
